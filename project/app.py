from flask import Flask, jsonify
import os
from datetime import datetime

app = Flask(__name__)
app.config.from_object('project.config.DevelopmentConfig')


@app.route('/entrypoint', methods=['GET'])
def entrypoint():
    return jsonify({
        'msg': 'hello world',
        'version': '1.4',
        'app_settings': os.getenv('APP_SETTINGS'),
        'host': os.uname()
    })


@app.route('/train', methods=['GET'])
def train():
    timestamp = datetime.utcnow().strftime('%Y-%m-%d_%H:%M:%S.%f')[:-3]
    os.makedirs('data/{}'.format(timestamp))
    print('Training model...')
    os.system('fortune > data/{}/model'.format(timestamp))
    return jsonify({
        'status': 0,
        'output': timestamp
    })
