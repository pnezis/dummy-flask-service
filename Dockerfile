FROM python:3-alpine

RUN sed -i -e 's/v3\.4/edge/g' /etc/apk/repositories
RUN apk update && apk add fortune

# Set working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Add requirements first
ADD ./requirements.txt /usr/src/app/requirements.txt

# Install requirements
RUN pip install -r requirements.txt

# Add application
ADD . /usr/src/app

# Run the server
CMD python manage.py runserver -h 0.0.0.0
